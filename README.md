# Github Issues Example

Demo consumption of the GitHub Issues API for the Angular project.

## Getting Started

### Prerequisites

The following must be installed:

* [node](https://nodejs.org/en/download/)
* [yarn](https://yarnpkg.com/lang/en/docs/install)

### Installing

1. Navigate to the project's root directory.

1. Execute the command `yarn`. This will install all of your dependencies.

### Running

1. Navigate to the project's root directory.

1. Execute the command `yarn start`. The development server should automatically
open your browser and bring you to `localhost:3000`, where the website is now
running.

## About this Project

This project is meant to showcase that I know how to consume RESTful APIs, as
well as have familiarity with React projects.

### Tools Used

#### React

React is the core of the app. It controls all the rendering and state management.

#### Bootstrap

Bootstrap provides a lot of basic CSS that has become the norm across the 
industry.

#### Axios

Axios is my favorite promise based request library.

#### Showdown

This was a new `npm` package for me, it's a markdown-to-html converter. It
works okay, but there's some idiosyncrasies with it.

## Author

* **John Mollberg** - *Sole author*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
