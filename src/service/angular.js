import axios from 'axios'

const ISSUES_BASE_URL = 'https://api.github.com/repos/angular/angular/issues'

const search = () => axios.get(ISSUES_BASE_URL, {
  headers: {
    'Accept': 'application/vnd.github.v3.full+json'
  }
})
  .then(response => response.data)
  .catch(() => [])

const get = number => axios.get(`${ISSUES_BASE_URL}/${number}`, {
  headers: {
    'Accept': 'application/vnd.github.v3.full+json'
  }
})
  .then(response => response.data)

export {
  search,
  get
}