import React, {Component} from 'react'
import logo from './logo.png'
import './App.css'
import * as api from './service/api'
import 'bootstrap'
import showdown from 'showdown'

const markdownConverter = new showdown.Converter()

const extractBody = issue => {
  return markdownConverter.makeHtml(issue.body)
}

const IssueRow = (issue, index) => {
  const headingId = `heading${index}`
  const collapseId = `collapse${index}`
  const collapseTarget = `#${collapseId}`

  return (
    <div className="card" key={index}>
      <div className="card-header p-0" id={headingId}>
        <h5 className="mb-0">
          <button className="btn full-width"
                  data-toggle="collapse"
                  data-target={collapseTarget}
                  aria-expanded="false"
                  aria-controls={collapseId}>
            <div className="card">
              <div className="card-body">
                <h5
                  className="card-title full-width text-left issue-title">{issue.title}</h5>
                <span
                  className='card-text float-left'>{issue.user.login}</span>
                <span className='card-text float-right'>
                  {
                    issue.assignee ?
                      issue.assignee.login :
                      'Unassigned'
                  }
                </span>
              </div>
            </div>
          </button>
        </h5>
      </div>
      <div id={collapseId} className="collapse"
           aria-labelledby={headingId} data-parent="#accordion">
        <div className="card-body text-left">
          <div dangerouslySetInnerHTML={{__html: extractBody(issue)}}/>
        </div>
      </div>
    </div>
  )
}

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      issues: []
    }
  }

  componentDidMount() {
    this.loadState()
  }

  loadState() {
    api.angular.search().then(issues => {
      this.setState({
        issues
      })
    })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo"/>
          <h1 className="App-title">I got 99 Issues, but they're neat and organized.</h1>
        </header>
        <div className='row'>
          <div className='col-xs-12 col-sm-10 offset-sm-1'>
            <div id="accordion" className='m-3'>
              {this.state.issues.map((issue, index) => IssueRow(issue, index))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default App
